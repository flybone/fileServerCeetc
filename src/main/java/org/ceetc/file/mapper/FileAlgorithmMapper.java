package org.ceetc.file.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.ceetc.file.entity.FileAlgorithm;
import org.springframework.stereotype.Repository;

@Mapper
public interface FileAlgorithmMapper {
    int deleteByPrimaryKey(Long id);

    int insert(FileAlgorithm record);

    int insertSelective(FileAlgorithm record);

    FileAlgorithm selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FileAlgorithm record);

    int updateByPrimaryKeyWithBLOBs(FileAlgorithm record);

    int updateByPrimaryKey(FileAlgorithm record);
}
